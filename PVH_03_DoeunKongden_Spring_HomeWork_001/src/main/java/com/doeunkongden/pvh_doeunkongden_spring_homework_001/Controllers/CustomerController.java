package com.doeunkongden.pvh_doeunkongden_spring_homework_001.Controllers;

import com.doeunkongden.pvh_doeunkongden_spring_homework_001.Entity.CustomerEntity;
import com.doeunkongden.pvh_doeunkongden_spring_homework_001.Entity.NewCustomer;
import com.doeunkongden.pvh_doeunkongden_spring_homework_001.Response.CustomerResponse;
import com.doeunkongden.pvh_doeunkongden_spring_homework_001.Response.InvalidDataResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {

    private final List<CustomerEntity> customerEntityList = new ArrayList<>();


    public CustomerController() {
        customerEntityList.add(new CustomerEntity("Kongden", "Male", 17, "Phnom Penh"));
        customerEntityList.add(new CustomerEntity("Momo", "Male", 18, "KPS"));
        customerEntityList.add(new CustomerEntity("Koko", "Female", 27, "PVH"));
    }

    @GetMapping("/")
    public ResponseEntity<?> getAllCustomers() {
        return ResponseEntity.ok(new CustomerResponse<List<CustomerEntity>>(
                "Success",
                customerEntityList,
                LocalDateTime.now(),
                "ok"
        ));
    }

    //Get Customer By Id
    @GetMapping("/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable("id") int id) {
        for (CustomerEntity customer : customerEntityList) {
            if (customer.getId() == id) {
                return ResponseEntity.ok(new CustomerResponse<CustomerEntity>(
                        "Success",
                        customer,
                        LocalDateTime.now(),
                        "ok"
                ));
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new InvalidDataResponse(
                "Customer Not Found",
                LocalDateTime.now(),
                "wrong id"
        ));
    }

    //Get Customer By Name
    @GetMapping("/search")
    public ResponseEntity<?> getCustomerByName(@RequestParam String name) {
        String validateInput = "[a-zA-Z]+";
        if (name.matches(validateInput)) {
            for (CustomerEntity customer : customerEntityList) {
                if (customer.getName().toLowerCase().contains(name.toLowerCase())) {
                    return ResponseEntity.ok(new CustomerResponse<CustomerEntity>(
                            "Success",
                            customer,
                            LocalDateTime.now(),
                            "ok"
                    ));
                }
            }
        } else {
            return ResponseEntity.badRequest().body(new InvalidDataResponse(
                    "Enter Valid Data",
                    LocalDateTime.now(),
                    "Not Valid Data"
            ));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new InvalidDataResponse(
                "Customer Not Found",
                LocalDateTime.now(),
                "wrong name"
        ));
    }

    //Inserting New Customer
    @PostMapping("/")
    public ResponseEntity<?> insertNewCustomer(@RequestBody NewCustomer customer) {
        String validateInput = "[a-zA-Z]+";
        if (customer.getName().matches(validateInput) && customer.getGender().matches(validateInput) && customer.getAddress().matches(validateInput)) {
            CustomerEntity customer1 = new CustomerEntity(customer.getName(), customer.getGender(), customer.getAge(), customer.getAddress());
            customerEntityList.add(customer1);
            return ResponseEntity.ok(new CustomerResponse<CustomerEntity>(
                    "Success Input",
                    customer1,
                    LocalDateTime.now(),
                    "ok"
            ));
        }else{
            return ResponseEntity.badRequest().body(new InvalidDataResponse(
                    "Enter Valid Data",
                    LocalDateTime.now(),
                    "Not Valid Data"
            ));
        }
    }

    //Update Customer
    @PutMapping("/{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable("id") int id, @RequestBody NewCustomer customer) {
        String validateInput = "[a-zA-Z]+";
        boolean found = false;

        for (CustomerEntity customer1 : customerEntityList) {
            if (customer1.getId() == id) {
                if (customer.getName().matches(validateInput) && customer.getGender().matches(validateInput) && customer.getAddress().matches(validateInput)) {
                    customer1.setName(customer.getName());
                    customer1.setGender(customer.getGender());
                    customer1.setAge(customer.getAge());
                    customer1.setAddress(customer.getAddress());
                    found = true;
                } else {
                    return ResponseEntity.badRequest().body(new InvalidDataResponse(
                            "Enter Valid Data",
                            LocalDateTime.now(),
                            "Not Valid Data"
                    ));
                }
            }
        }
        if (!found) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new InvalidDataResponse(
                    "Customer Not Found",
                    LocalDateTime.now(),
                    "wrong id"
            ));
        }

        return ResponseEntity.ok(new CustomerResponse<NewCustomer>(
                "Success",
                customer,
                LocalDateTime.now(),
                "ok"
        ));
    }


    //Deleting Customer
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable("id") int id) {
        for (CustomerEntity customer : customerEntityList) {
            if (customer.getId() == id) {
                customerEntityList.remove(customer);
                return ResponseEntity.ok(new CustomerResponse<CustomerEntity>(
                        "Delete Successful",
                        customer,
                        LocalDateTime.now(),
                        "ok"
                ));
            }
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new InvalidDataResponse(
                "Customer Not Found",
                LocalDateTime.now(),
                "wrong id"
        ));
    }
}
