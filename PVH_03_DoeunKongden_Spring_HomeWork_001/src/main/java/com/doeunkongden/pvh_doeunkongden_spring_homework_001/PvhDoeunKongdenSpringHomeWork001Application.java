package com.doeunkongden.pvh_doeunkongden_spring_homework_001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PvhDoeunKongdenSpringHomeWork001Application {

    public static void main(String[] args) {
        SpringApplication.run(PvhDoeunKongdenSpringHomeWork001Application.class, args);
    }

}
