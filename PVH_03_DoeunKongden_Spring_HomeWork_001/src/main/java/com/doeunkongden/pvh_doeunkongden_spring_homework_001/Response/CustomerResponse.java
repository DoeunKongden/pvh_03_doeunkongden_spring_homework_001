package com.doeunkongden.pvh_doeunkongden_spring_homework_001.Response;

import java.time.LocalDateTime;

public class CustomerResponse<T> {
    private String message;
    private T customer;
    private LocalDateTime localDateTime;
    private String status;

    public CustomerResponse(String message, T customer, LocalDateTime localDateTime, String status) {
        this.message = message;
        this.customer = customer;
        this.localDateTime = localDateTime;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getCustomer() {
        return customer;
    }

    public void setCustomer(T customer) {
        this.customer = customer;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
