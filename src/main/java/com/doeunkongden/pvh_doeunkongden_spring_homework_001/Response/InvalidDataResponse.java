package com.doeunkongden.pvh_doeunkongden_spring_homework_001.Response;

import java.time.LocalDateTime;

public class InvalidDataResponse {
    private String message;
    private LocalDateTime localDateTime;
    private String status;

    public InvalidDataResponse(String message, LocalDateTime localDateTime, String status) {
        this.message = message;
        this.localDateTime = localDateTime;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
