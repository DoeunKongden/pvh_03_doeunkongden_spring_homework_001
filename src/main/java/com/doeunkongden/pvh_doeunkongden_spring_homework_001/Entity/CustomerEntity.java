package com.doeunkongden.pvh_doeunkongden_spring_homework_001.Entity;

public class CustomerEntity {

    private int id;
    private String name;
    private String gender;
    private int age;
    private String address;
    private  static  int num=1;

    public CustomerEntity(String name, String gender, int age, String address) {
        this.id=num;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.address = address;
        num++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
